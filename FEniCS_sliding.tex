\documentclass{article}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{listings}

% python package from SO. Nice
% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{txtt}{bx}{n}{12} % for bold
\DeclareFixedFont{\ttm}{T1}{txtt}{m}{n}{12}  % for normal

% Custom colors
\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}

\usepackage{listings}

% Python style for highlighting
\newcommand\pythonstyle{\lstset{
language=Python,
basicstyle=\ttfamily\small,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false            % 
}}


% Python environment
\lstnewenvironment{python}[1][]
{
\pythonstyle
\lstset{#1}
}
{}

% Python for external files
\newcommand\pythonexternal[2][]{{
\pythonstyle
\lstinputlisting[#1]{#2}}}

% Python for inline
\newcommand\pythoninline[1]{{\pythonstyle\lstinline!#1!}}

\title{Equations and boundary conditions used in ice flow}

\author{Christian HELANOW\\
Department of Physical \\
Stockholm University\\
E-mail: christian.helanow@natgeo.su.se\\
}


\begin{document}
\maketitle

\section*{Introduction}
Below follows a short summary of the equations used in ice flow. The most accurate representation of ice flow in glacier/ice sheet modeling is the stationary Navier-Stokes (Stokes) equations. For time dependent flow, the surface/mesh is updated by solving the kinematic condition at the free surface. In general, there is a stress-free condition at the top surface/boundary, and at the bottom the ice is either frozen to the underlying bed (zero velocity), sliding (slip condition) or floating (hydrostatic normal stresses).

\section*{Equation}
Stokes flow:
\begin{equation}
\frac{\partial\tau_{ij}}{\partial x_j} - \frac{\partial p}{\partial x_i} + \rho g_i = 0,
\label{eq:Stokes}
\end{equation}
where $\tau_{ij}$ is the deviatoric stress tensor, $p$ is the pressure, $\rho$ ice density and $g_i = (0,0,-g)$ is gravity.\\
The constitutive relation used is that for a non-newtonian fluid
\[
\tau_{ij} = 2\mu(\dot{\varepsilon}^2)\dot{\varepsilon}_{ij},
\]
where $\dot{\varepsilon}_{ij}$ is the strain-rate tensor
\[
\dot{\varepsilon}_{ij} = \frac{1}{2}\left(\frac{\partial u_i}{\partial x_j} + \frac{\partial u_j}{\partial x_i}  \right).
\]
The velocity is denoted by $u_i$ and $\dot{\varepsilon}^2 = \dot{\varepsilon}_{ij}\dot{\varepsilon}_{ij}$ is the second invariant of the strain-rate tensor.\\
The viscosity is is given by
\[
\mu(\dot{\varepsilon}^2) = \mu_0[\dot{\varepsilon}^2]^{(1-n)/2n},
\]
which is called Glen's flow law in the glaciological literature and where $n=3$. In general, $\mu_0$ is temperature dependent, but I will not include the temperature couplings in this version.

\section*{Boundary conditions}
With the total stress tensor $\sigma_{ij} = \tau_{ij} -p\delta_{ij}$ and the outward oriented surface normal $n_i$, the surface boundary condition is
\[
\sigma_{ij}n_j = 0.
\]
The simplest bottom boundary condition is that the ice is frozen to the bed,
\[
u_i = 0.
\]
A more general case is that of the impenetrability condition with slip, which gives
\begin{subequations}
\label{eq:bottom_bc}
\begin{eqnarray}
u_jn_j = 0, && \textnormal{(impenetrability)}\label{eq:imp}\\
\left( \sigma_{ij}n_j \right)^{\parallel} = - \beta u_i, && \textnormal{(sliding tangential to bottom)}\label{eq:slide}
\end{eqnarray}
\end{subequations}
where $\beta$ is a positive sliding coefficient and the $^{\parallel}$ indicates the tangential components.\\
If the ice should be floating on water, on would have
\[
\sigma_{ij}n_j = -p_wn_i,
\]
where $p_w$ is the water pressure.

\subsection*{Previous/current implementations of bottom boundary conditions}
From what I have seen (in the case of ice), the previous implementations of eq. \eqref{eq:bottom_bc}, has been with the use of a lagrange multiplier along the parts of the boundary identified with the bottom. What's published as the numerical model VarGlas (built upon the FEniCS framework), the weak form of the Stokes' equations with the incompressibility constraint results in:

\begin{subequations}
\begin{eqnarray}
&&\int_{\Omega} \boldsymbol{\tau}:\nabla\mathbf{v} - p\nabla\cdot\mathbf{v} d\Omega\nonumber\\
    &&- \int_{\Omega} q\nabla\cdot\mathbf{u} - \rho\mathbf{g} d\Omega\nonumber\\
    &&+ \int_{\Gamma_b} q\mathbf{u}\cdot\mathbf{n} + p\mathbf{v}\cdot\mathbf{n} d\Gamma_b\label{eq:var_imp}\\
    &&+ \int_{\Gamma_b} \beta \mathbf{u}\cdot\mathbf{v} d\Gamma_b = 0\label{eq:var_slide}.
\end{eqnarray}
\end{subequations}

Here, the $(\mathbf{u},p)$ are the the velocity and pressure functions and $(\mathbf{v},q)$ are the corresponding test functions. Above, the weak forms of eq. \eqref{eq:imp} and \eqref{eq:slide} are given by eq. \eqref{eq:var_imp} and \eqref{eq:var_slide}.

Me, personally, I am unsure regarding the above implementation, since it does not seem to add up to the specified boundary conditions at the bottom (if one considers the weak form as the result of minimizing an energy functional).

\subsection{My current approach}
The approach I tried, that I hope (at least from the continuous perspective) gives the right boundary conditions is quite similar to the above, but instead introduces a separate lagrange multiplier $\Lambda$ (with test function $\Psi$) on the bottom boundary.

\begin{subequations}
\begin{eqnarray}
&&\int_{\Omega} \boldsymbol{\tau}:\nabla\mathbf{v} - p\nabla\cdot\mathbf{v} d\Omega\nonumber\\
    &&- \int_{\Omega} q\nabla\cdot\mathbf{u} - \rho\mathbf{g} d\Omega\nonumber\\
    &&+ \int_{\Gamma_b} \Psi\mathbf{u}\cdot\mathbf{n} + \Lambda\mathbf{v}\cdot\mathbf{n} d\Gamma_b\label{eq:lag_imp}\\
    &&+ \int_{\Gamma_b} \beta \mathbf{u}\cdot\mathbf{v} d\Gamma_b = 0\label{eq:lag_slide}.
\end{eqnarray}
\end{subequations}

In FEniCS I imposed this condition by adding the lagrange multiplier to the \pythoninline{MixedFunctionSpace} defined as

\begin{python}
  T = FunctionSpace(mesh, ''Discontiuous Lagrange Trace'', 1)
  DirichletBC(T, 0, boundary_function, 0),
\end{python}

where the \pythoninline{boundary_function} above marks 0 for all facets NOT on the bottom boundary. This approach sometimes behaves strangely, depending on whether $\beta$ is a \pythoninline{Expression} or a \pythoninline{Function}. Not sure why, I haven't finished my investigations.

Another approach I tried, was to use the Nitsche penalty method for impenetrability. This works (in the ``eye norm'') fine for linear Stokes', but couldn't get it working for the nonlinear scenario above.

\section{Strong implementation of impenetrability}

Of course, I would very much like to compare both of the above to a reference solution. In my mind, there is no better way than to do have impenetrability imposed strongly, and sliding weakly.

I also have put some effort into having a manufactured solution for the nonlinear Stokes' equations for ice, with sliding conditions. It would be fun to compare all the above to that, if possible.
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
