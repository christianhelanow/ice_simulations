#! /usr/bin/env python
"""
Demo for ice flow in the form of nonlinear Stokes equations.
Frozen to bed for simplicity, since this circumvents the issue of
implementing a strong boundary condition for impenetrability.
"""

__author__ = "Christian Helanow (christian.helanow@natgeo.su.se)"
__copyright__ = "Copyright (c) 2015 %s" % __author__

from fenics import *
import sys
from time import time
print "Starting simulation"

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 2
# parameters["form_compiler"]["cpp_optimize_flags"] = '-O2 -funroll-loops'

# choose dimension of simulation and periodic bcs
periodic = True               # if periodic bc's should be used
Vdim = 2                      # dimension of velocity space
Pdim = 1                        # dimesnion of pressure space

# constants
rho = 910
g = 9.81
A0 = 10**(-16)             # ice softness Pa^3/year
n_i = 3.                        # flow exponent
# ice hardness
mu0 = A0**(-1. / n_i) * .5**((1 + n_i) / (2 * n_i))


# read mesh from

# mesh = Mesh("storglaciaren.xml")     # customized mesh topo
nx3 = 20
ny3 = 20
nz3 = 3
Lx, Ly, Lz = 4000, 1000, 1000
mesh = RectangleMesh(0., 0., Lx, Ly, nx3, ny3)
# mesh = BoxMesh(0., 0., 0., Lx, Ly, Lz, nx3, ny3, nz3)
angle = .5 * pi / 180

# mesh variables (processor wise for parallel)
xmin = MPI.min(mpi_comm_world(), mesh.coordinates()[:,0].min() )
xmax = MPI.max(mpi_comm_world(), mesh.coordinates()[:,0].max() )
ymin = MPI.min(mpi_comm_world(), mesh.coordinates()[:,1].min() )
ymax = MPI.max(mpi_comm_world(), mesh.coordinates()[:,1].max() )

dim = mesh.geometry().dim()     # dimension of domain


# body force
if dim == 2:
    Force = Constant((sin(angle) * g * rho, -g * cos(angle) * rho))
elif dim == 3:
    Force = Constant((sin(angle) * g * rho, 0.,  -g * cos(angle) * rho))
else:
    print "Domain needs to be either 2D or 3D"
    sys.exit()



# SUB DOMAIN FOR PERIODIC BOUNDARY CONDITION

if dim == 2:
    class PeriodicBoundary(SubDomain):
        # Left boundary is "target domain" G

        def inside(self, x, on_boundary):
            return near(x[0], xmin) and on_boundary

        # Map right boundary (H) to bc1 boundary (G)
        def map(self, x, y):
            y[0] = x[0] - xmax
            y[1] = x[1]
else:
    class PeriodicBoundary(SubDomain):

        def inside(self, x, on_boundary):
            """
            Return True if on left or bottom boundary AND NOT on one 
            of the two corners (0, 1) and (1, 0).
            """
            return bool((near(x[0], xmin) or near(x[1], ymin)) and
                        (not ((near(x[0], xmin) and near(x[1], ymax))
                              or (near(x[0], xmax) and near(x[1], ymin))))
                        and on_boundary)

        def map(self, x, y):
            """
            Remap the values on the top and right sides to the bottom and left
            sides.
            """
            if near(x[0], xmax) and near(x[1], ymax):
                y[0] = x[0] - xmax
                y[1] = x[1] - ymax
                y[2] = x[2]
            elif near(x[0], xmax):
                y[0] = x[0] - xmax
                y[1] = x[1]
                y[2] = x[2]
            elif near(x[1], ymax):
                y[0] = x[0]
                y[1] = x[1] - ymax
                y[2] = x[2]
            else:
                y[0] = x[0]
                y[1] = x[1]
                y[2] = x[2]
     



# Define function spaces

if periodic == True:
    pbc = PeriodicBoundary()
    V = VectorFunctionSpace(mesh, 'CG', Vdim, constrained_domain=pbc)
    Q = FunctionSpace(mesh, 'CG', Pdim, constrained_domain=pbc)
    # mini elements
    P1 = VectorFunctionSpace(mesh, "Lagrange", 1, constrained_domain=pbc)
    B  = VectorFunctionSpace(mesh, "Bubble", 3, constrained_domain=pbc)
    # crouziex raviart
    CR = VectorFunctionSpace(mesh, "CR", 1, constrained_domain=pbc)
else:
    V = VectorFunctionSpace(mesh, 'CG', Vdim)
    Q = FunctionSpace(mesh, 'CG', Pdim)
W = V * Q
# mini function space
W = (P1 + B)*Q
# CR
W = CR*Q

# MARK BOUNDARIES
mesh.init()                     # initiate mesh connectivity
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
# mark boundaries of bottom as 1
# this assumes that the "sides" are nearly vertical (less inclination than
# 10*DOLFIN_EPS)
for facet in facets(mesh):
    # use dim = 2 or 3 to access the right index of the coordinate system
    # assuming that if 2d the coordinate x is the horizontal (y points up)
    # and if 3d the plane x-y is horizontal (z points up)
    if facet.exterior() and facet.normal()[dim - 1] < -10 * DOLFIN_EPS:
        boundaries[facet] = 1

ds = Measure("ds")[boundaries]

# BOUNDARY CONDITION
no_slip = DirichletBC(W.sub(0), Constant((0., 0.)), boundaries, 1)
bcs = no_slip

# Trial and Test functions
w = TrialFunction(W)
z = TestFunction(W)
u, p = split(w)
v, q = split(z)

n = FacetNormal(mesh)           # facet normal


def f(v): return inner(Force, v) * dx


def epsdot(u): return inner(
    sym(grad(u)), sym(grad(u))) + 1e-15


def mu(u):
    return mu0 * (epsdot(u))**((1 - n_i) / (2 * n_i))  # Dukowicz


def tau(u):
    return 2 * mu(u) * sym(grad(u))


def a(u, v): return inner(tau(u), grad(v)) * dx


def t(u, p): return dot(2 * mu_stab * sym(grad(u)), n) - p * n  # dot(sigma,n)


# VARIATIONAL FORMULATIONS

# 'Normal' part of variational formulation with stabilization parameter alpha
A = a(u, v) \
    - div(v) * p * dx \
    - div(u) * q * dx \
    - f(v)

stokes_DBC = A
wh0 = Function(W)
F = action(stokes_DBC, wh0)
J = derivative(F, wh0, w)
# parameters['linear_algebra_backend'] = 'Eigen'
problem = NonlinearVariationalProblem(F, wh0, bcs, J)
solver = NonlinearVariationalSolver(problem)
prm = solver.parameters
### SETTING FOR DIRECT LINEAR SOLVER
prm['newton_solver']['linear_solver'] = 'petsc'
prm['newton_solver']['method'] = 'lu'

### SETTINGS FOR NONLINEAR SOLVER SNES
# prm['nonlinear_solver'] = 'snes'
# prm['snes_solver']['line_search'] = 'cp'
# prm['snes_solver']['maximum_iterations'] = 200
# prm['snes_solver']['linear_solver'] = 'gmres'
# prm['snes_solver']['preconditioner'] = 'ilu'

### SETTING FRO NONLINEAR SOLVER NEWTON
# prm['nonlinear_solver'] = 'newton'
# prm['newton_solver']['linear_solver'] = 'gmres'
# prm['newton_solver']['preconditioner'] = 'ilu'

### GENERAL SETTINGS FOR NEWTON SOLVER
prm['newton_solver']['maximum_iterations'] = 200
prm['newton_solver']['relaxation_parameter'] = 1.

### NONZERO INITIAL GUESS
# prm['newton_solver']['krylov_solver']['nonzero_initial_guess'] = True
# prm['newton_solver']['krylov_solver']['monitor_convergence'] = True
# problem.solution().assign(Function(W, "saved_solution.xml"))  # init guess

# sys.exit()
set_log_level(INFO)
start = time()
solver.solve()
print "Simulation took {} seconds".format(time() - start)
uh0, ph0 = wh0.split()
File("demo_ice_flow/velocity.pvd") << uh0
File("demo_ice_flow/pressure.pvd") << ph0

uh0 = project(uh0)
File("demo_ice_flow/facets.pvd") << boundaries
File("demo_ice_flow/Viscosity.pvd") << (project(mu0 * 
                                        inner(sym(grad(uh0)),
                                        sym(grad(uh0)))**((1 - n_i) / (2 * n_i)),
                                        solver_type = "cg"))

