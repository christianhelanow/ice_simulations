(TeX-add-style-hook
 "Notes"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "graphicx"
    "amsmath"
    "mathtools"
    "xfrac"
    "nicefrac"
    "caption"
    "subcaption")
   (LaTeX-add-labels
    "eq:IP"
    "fig:dome_s0"
    "fig:dome_s1"
    "fig:dome_unstruct")))

